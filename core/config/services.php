<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/
	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],
	'mandrill' => [
		'secret' => '',
	],
	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],
	'stripe' => [
		'secret' => '',
		'status' => '',
		'key' => '',
	],
    'paypal'=>[
		'client_id' => '',
		'secret' => '',
		'status' => '',
		'account' => '',
		'mode' => '',
	],
    'license'=>[
		'is_verified' => '1',
		'purchase_code' => '1de4f343-2ae7-4d61-9c16-e00ad3371bd5',
	]
];
